<?php

namespace Desk\EventsBundle\Controller;

use Desk\EventsBundle\Entity\Events;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller {

    /**
     * @return Response
     */
    public function indexAction() {
        return $this->render( 'DeskEventsBundle:Default:index.html.twig' );
    }

    /**
     * Events list
     * @return Response
     */
    public function eventsAction() {
        $em = $this->getDoctrine()->getManager();

        $arEvents = [];

        /** @var Events $event */
        foreach ( $em->getRepository( 'DeskEventsBundle:Events' )->findAll() as $key => $event ) {
            $arEvents[$key] = ['id'    => $event->getId(),
                               'title' => $event->getTitle(),
                               'start' => $event->getEventDate()->format( 'Y-m-d' ),
                               'end'   => $event->getEventDate()->format( 'Y-m-d' )];
        }

        return new Response( json_encode( $arEvents ), 200, ['Content-Type' => 'application/json'] );
    }

    /**
     * @return Response
     */
    public function getAction() {
        /** @var Request $request */
        $request = $this->get( 'request' );
        $em      = $this->getDoctrine()->getManager();

        $id = $request->query->get( 'id' );
        /** @var Events $event */
        $event = $em->getRepository( 'DeskEventsBundle:Events' )->find( $id );
        if ( !$event ) {
            return new Response( '', 404, ['Content-Type' => 'application/json'] );
        }
        else {
            return new Response( json_encode( ['id'    => $event->getId(),
                                               'start' => $event->getEventDate()->format('Y-m-d'),
                                               'time'  => $event->getEventTime()->format('H:i'),
                                               'title' => $event->getTitle(),
                                               'description' => $event->getDescription(),
            ] ) );
        }
    }


    /**
     * @return Response
     */
    public function addEventAction() {
        /** @var Request $request */
        $request = $this->get( 'request' );
        $em      = $this->getDoctrine()->getManager();

        if ( $request->isMethod( 'POST' ) ) {
            $arPostData = $request->request->all();
            $dateEvent  = new \DateTime( $arPostData['dateEvent'] );
            $timeEvent  = new \DateTime( $arPostData['timeEvent'] );

            $oEvents = new Events();
            $oEvents->setEventDate( $dateEvent );
            $oEvents->setEventTime( $timeEvent );
            $oEvents->setTitle( $arPostData['title'] );
            $oEvents->setUid( 0 );
            $oEvents->setDescription( $arPostData['eventDescription'] );

            $em->persist( $oEvents );
            $em->flush();
            return new Response( json_encode( ['id'    => $oEvents->getId(),
                                               'start' => $arPostData['dateEvent'],
                                               'end'   => $arPostData['dateEvent'],
                                               'title' => $arPostData['title'],
                                               'description' => $arPostData['eventDescription'],
            ]), 200, ['Content-Type' => 'application/json'] );
        }
        else {
            return new Response( 'fail', 403, ['Content-Type' => 'application/json'] );
        }
    }

    /**
     * @return Response
     */
    public function updateAction() {
        $request = $this->get( 'request' );
        /** @var EntityManager $em */
        $em      = $this->getDoctrine()->getManager();

        if ( $request->isMethod( 'POST' ) ) {
            $arPostData = $request->request->all();
            $dateEvent  = new \DateTime( $arPostData['dateEvent'] );
            $timeEvent  = new \DateTime( $arPostData['timeEvent'] );

            $oEvents = $em->getRepository('DeskEventsBundle:Events')->find($arPostData['id']);
            $oEvents->setEventDate( $dateEvent );
            $oEvents->setEventTime( $timeEvent );
            $oEvents->setTitle( $arPostData['title'] );
            $oEvents->setUid( 0 );
            $oEvents->setDescription( $arPostData['eventDescription'] );

            $em->persist( $oEvents );
            $em->flush();
            return new Response( json_encode( ['id'    => $oEvents->getId(),
                                               'start' => $arPostData['dateEvent'],
                                               'end'   => $arPostData['dateEvent'],
                                               'title' => $arPostData['title'],
                                               'description' => $arPostData['eventDescription']
            ]), 200, ['Content-Type' => 'application/json'] );
        }
        else {
            return new Response( 'fail', 404, ['Content-Type' => 'application/json'] );
        }
    }

    public function deleteAction() {
        $request = $this->get( 'request' );
        /** @var EntityManager $em */
        $em      = $this->getDoctrine()->getManager();

        if ( $request->isMethod( 'POST' ) ) {
            $arPostData = $request->request->all();
            $oEvents = $em->getRepository('DeskEventsBundle:Events')->find($arPostData['id']);
            if( !$oEvents ) {
                return new Response( 'fail', 404, ['Content-Type' => 'application/json'] );
            } else {
                $em->remove($oEvents);
                $em->flush();
                return new Response( $arPostData['id'], 200, ['Content-Type' => 'application/json'] );
            }
        }
    }
}

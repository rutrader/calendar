/**
 * Created by rishemgulov on 18.06.15.
 */

$(document).ready(function () {
    $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'prevYear, nextYear'
            },
            defaultDate: '2015-06-10',
            editable: true,
            eventLimit: true,
            selectable: true,
            selectHelper: true,

            //
            eventClick: function(callEvent, jsEvent, view) {
                event_id.val( callEvent._id );
                $.ajax({
                    type: 'GET',
                    url: '/app_dev.php/events/get',
                    data: {
                        id: callEvent._id
                    },
                    success: function( data ) {
                        data = JSON.parse(data);
                        event_date.val(data.start);
                        event_time.val(data.time);
                        event_title.val(data.title);
                        event_id.val(data.id);
                        event_description.val(data.description);
                        $('#add').hide();
                        $('#edit').hide();
                        formOpen('show');
                    }
                });
            },
            eventSources: [{
                url: '/app_dev.php/events/list',
                type: 'GET'
            }],
            dayClick: function(date, jsEvent, view) {
                $('#event_date').val(date.format());
                formOpen('add');
            },
            eventDrop: function(event, jsEvent, view) {
                event_id.val( event._id );
                $.ajax({
                    type: 'GET',
                    url: '/app_dev.php/events/get',
                    data: {
                        id: event._id
                    },
                    success: function( data ) {
                        data = JSON.parse(data);
                        event_date.val(event.start.format());
                        event_time.val(data.time);
                        event_title.val(data.title);
                        event_id.val(data.id);
                        event_description.val(data.description);
                        formOpen('edit');
                    }
                });
            }
        }
    );

    var event_date = $('#event_date');
    var event_time = $('#event_time');
    var event_title = $('#event_title');
    var event_description = $('#event_description');
    var calendar = $('#calendar');
    var form = $('#dialog-form');
    var event_id = $('#uid');
    var format = "Y-m-d";

    event_date.datetimepicker({format: format});
    event_time.datetimepicker({format: 'H:i', datepicker:false });


    /* кнопка добавления события */
    $('#add_event_button').button().click(function(){
        formOpen('add');
    });
    /** функция очистки формы */
    function emptyForm() {
        event_date.val("");
        event_time.val("");
        event_title.val("");
        event_id.val("");
        event_description.val("");
    }
    function closeForm() {
        form.dialog('close');
    }
    /* режимы открытия формы */
    function formOpen(mode) {
        if(mode == 'add') {
            /* скрываем кнопки Удалить, Изменить и отображаем Добавить*/
            $('#add').show();
            $('#edit').hide();
            $("#delete").button("option", "disabled", true);
        }
        else if(mode == 'edit') {
            /* скрываем кнопку Добавить, отображаем Изменить и Удалить*/
            $('#edit').show();
            $('#add').hide();
            $("#delete").button("option", "disabled", false);
        } else if( 'mode' == 'show' ) {
            $('#add').show();
            $('#edit').hide();
            $("#delete").button("option", "disabled", true);
        }
        form.dialog('open');
    }

    form.dialog({
        autoOpen: false,
        buttons: [{
            id: 'add',
            text: 'Добавить',
            click: function() {
                $.ajax({
                    type: 'POST',
                    url: '/app_dev.php/events/add',
                    data: {
                        dateEvent: event_date.val(),
                        timeEvent: event_time.val(),
                        title: event_title.val(),
                        eventDescription: event_description.val()
                    },
                    success: function(data) {
                        calendar.fullCalendar('renderEvent', {
                            id: data.id,
                            title: data.title,
                            start: data.start,
                            end: data.end
                        });
                    }
                });
                emptyForm();
                closeForm();
            },
            disabled: false
        },
            {   id: 'edit',
                text: 'Изменить',
                click: function() {
                    $.ajax({
                        type: 'POST',
                        url: '/app_dev.php/events/update',
                        data: {
                            id: event_id.val(),
                            dateEvent: event_date.val(),
                            timeEvent: event_time.val(),
                            title: event_title.val(),
                            eventDescription: event_description.val()
                        },
                        success: function(data) {
                            calendar.fullCalendar('refetchEvents');
                        }
                    });
                    $(this).dialog('close');
                    emptyForm();
                }
            },
            {   id: 'cancel',
                text: 'Отмена',
                click: function() {
                    $(this).dialog('close');
                    emptyForm();
                }
            },
            {   id: 'delete',
                text: 'Удалить',
                click: function() {
                    $.ajax({
                        type: 'POST',
                        url: '/app_dev.php/events/delete',
                        data: {
                            id: event_id.val()
                        },
                        success: function(data) {
                            calendar.fullCalendar('removeEvents', data );
                        }
                    });
                    $(this).dialog('close');
                    emptyForm();
                },
                disabled: true
            }]
    });

});
